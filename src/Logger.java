public class Logger {

    private static Logger instance;

    static {
        instance = new Logger();
    }

    private Logger() {
    }

    public static Logger getInstance(){
        return instance;
    }


    void log(String message) {
        System.out.println("Вы написали: " + message);
    }
}
